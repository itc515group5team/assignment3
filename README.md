# README #

### What is this repository for? ###

* ITC515 Assignment 3
* Version 1.0

### How do I get set up? ###

* Just clone the project and you should be ready to go

### Contribution guidelines ###

* Before pushing the code, please create a pull request before the changes can be made to staging and later we will do merge to the master.
* Code review to be done before merging feature branch code to staging.

### Who do I talk to? ###

* Project Team Members:
* Soviet Ligal (soviet.ligal@gmail.com)
* Olimjon Normukhamedov (alimjan7melbourne@gmail.com)
* Dinesh Vaidya (mave.vaidhya@gmail.com)