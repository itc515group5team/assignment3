package hotel.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.Occurs;
import hotel.credit.CreditCard;
import hotel.utils.IOUtils;

public class Room {

	private enum State {READY, OCCUPIED}

	int id;
	RoomType roomType;
	List<Booking> bookings;
	State state;
	Booking booking;


	public Room(int id, RoomType roomType) {
		this.id = id;
		this.roomType = roomType;
		bookings = new ArrayList<>();
		state = State.READY;
	}


	public String toString() {
		return String.format("Room : %d, %s", id, roomType);
	}


	public int getId() {
		return id;
	}

	public String getDescription() {
		return roomType.getDescription();
	}


	public RoomType getType() {
		return roomType;
	}

	public boolean isAvailable(Date arrivalDate, int stayLength) {
		IOUtils.trace("Room: isAvailable");
		for (Booking b : bookings) {
			if (b.doTimesConflict(arrivalDate, stayLength)) {
				return false;
			}
		}
		return true;
	}


	public boolean isReady() {
		return state == State.READY;
	}


	public Booking book(Guest guest, Date arrivalDate, int stayLength, int numberOfOccupants, CreditCard creditCard) {
		booking = new Booking(guest, this, arrivalDate, stayLength, numberOfOccupants, creditCard);
		booking.setStateToPending();
		bookings.add(booking); // this is required line, to make isAvailable() method to work, bookings arraylist needs to be populated everytime a new room booking is made.
		// then it would be possible to check if a time conflicts. But still it is not enough, as this arraylist needs to be static
		// This app uses less static methods, so bookings could be stored and the data is reused.
		return booking;
	}


	public void checkin() {
	    if (this.state != State.READY){
	        throw new RuntimeException("This room is not ready yet");
        }
        this.state = State.OCCUPIED;
	    booking.setStateToCheckIn();
	}


	public void checkout(Booking booking) {
		if(this.state != State.OCCUPIED){
		    throw new RuntimeException("This room is not occupied");
        }
        this.state = State.READY;
		booking.setStateToCheckOut();
	}

	public boolean isOccupied(){
		return this.state == State.OCCUPIED;
	}

    public void setStateToOccupied(){
        state = State.OCCUPIED;
    }

    public void setStateToReady(){
        state = State.READY;
    }


}
