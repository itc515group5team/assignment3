import hotel.booking.BookingCTL;
import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Hotel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.lang.reflect.Executable;

import static org.junit.jupiter.api.Assertions.*;

class BookingCTLTest {

    @InjectMocks
    BookingCTL bookingCTL = new BookingCTL(new Hotel());

    @Mock
    CreditCardType creditCardType;

    @Mock
    int creditNumber;

    @Mock
    int ccv;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void creditDetailsEntered() {

        Throwable exception = assertThrows(RuntimeException.class,
                () -> {bookingCTL.creditDetailsEntered(creditCardType, creditNumber, ccv);} );

        assertEquals("The state is not set to CREDIT", exception.getMessage());

        // Private state enum does not let fully proceed the method
    }

    @Test
    void creditDetailsEnteredAuthorized() {

    }

    @Test
    void creditDetailsEnteredDenied() {

    }
}