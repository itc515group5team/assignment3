import hotel.credit.CreditCard;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.lang.reflect.Executable;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {
    int roomId = 1;

    RoomType roomType = RoomType.SINGLE;

    @Mock
    Guest guest;
    Room room = new Room(roomId, roomType);

    @Mock
    ServiceType serviceType;

    Date arrivalDate = new Date(2018,9,30);
    @Spy int stayLength;
    @Spy int numberOfOccupants;
    @Spy double cost;

    @Mock
    CreditCard creditCard;

    @InjectMocks Booking booking = new Booking(guest, room, arrivalDate, stayLength, numberOfOccupants, creditCard);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void checkIn() {
        booking.checkIn();
        assertTrue(booking.isCheckedIn());
    }

    @Test
    void addServiceCharge() {
        booking.addServiceCharge(serviceType, cost);
    }

    @Test
    void checkOut() {
        booking.checkIn();
        assertTrue(booking.isCheckedIn(),"Booking State is not checked in");
        booking.checkOut();
        assertTrue(booking.isCheckedOut());

    }
}