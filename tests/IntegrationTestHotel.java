import hotel.HotelHelper;
import hotel.booking.BookingCTL;
import hotel.booking.BookingUI;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.exceptions.CancelException;
import hotel.utils.IOUtils;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static hotel.booking.BookingUI.State.PHONE;
import static hotel.booking.BookingUI.State.REGISTER;
import static org.junit.jupiter.api.Assertions.*;

public class IntegrationTestHotel  {

    public Date initializeArrivalDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = format.parse("01-01-0001");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        cal.setTime(date);

        return date;

    }


    @Test
    public void OneUsersBooksWithTheSameArrivalDate() {
        Hotel hotel = populateHotel();
        IOUtils.outputln("OneUsersBooksWithTheSameArrivalDate:\n--------");

        Date date = initializeArrivalDate();

        addHotelRoom(hotel); //adds hotel rooom to hotel
        IOUtils.outputln("Populating rooms...");

        Guest guest = new Guest("Fred", "Nurke", 2);

        CreditCard card = new CreditCard(CreditCardType.VISA, 2, 2);

        Room room = hotel.findAvailableRoom(RoomType.TWIN_SHARE, date, 1);

        //1. hotel booking tests goes here
        long confNo = hotel.book(room, guest, date, 1, 2, card);
        IOUtils.outputln(guest.getName() + " has booked a room date: " + date + " \nconfirmation Number: " + confNo);

        Booking booking = hotel.findBookingByConfirmationNumber(confNo);

        assertNotNull(booking, "Booking exists for this room");

        //2. checkin tests follows here
        hotel.checkin(confNo);
        IOUtils.outputln(guest.getName() + " has checked in");
        Booking activeBooking = hotel.findActiveBookingByRoomId(booking.getRoomId());

        //2.1 The Booking referenced by confirmationNumber should be returned by getActiveBookingByRoomId()
        assertEquals(booking, activeBooking);

        //2.2 The Booking referenced by confirmationNumber should have a state of CHECKED_IN
        Boolean checkedIn = booking.isCheckedIn();

        assertEquals(true, checkedIn);

        //3  add service charge tests goes here
        booking.addServiceCharge(ServiceType.ROOM_SERVICE, 7.00);
        IOUtils.outputln(guest.getName() + " has added a service charge");

        //3.1 A ServiceCharge should have been added to the active booking.
        List<ServiceCharge> serviceChargeList = activeBooking.getCharges();
        assertNotNull(serviceChargeList);

        //4. checkout tests goes here
        hotel.checkout(booking.getRoomId());
        IOUtils.outputln(guest.getName() + " has checked out");

        Boolean checkedOut = booking.isCheckedOut();

        //4.1 The Booking referenced by confirmationNumber should have a state of CHECKED_OUT
        assertEquals(true, checkedOut);

        IOUtils.outputln("-------------");
    }
    @Test
    public void TwoUsersBooksWithTheSameArrivalDate() {

        Hotel hotel = populateHotel();

        IOUtils.outputln("TwoUsersBooksWithTheSameArrivalDate:\n--------");

        Date date = initializeArrivalDate();

        addHotelRoom(hotel); //adds hotel rooom to hotel
        IOUtils.outputln("Populating rooms...");

        Guest guest = new Guest("Alex", "Wolongolong", 2);

        CreditCard card = new CreditCard(CreditCardType.VISA, 2, 2);

        Room room = hotel.findAvailableRoom(RoomType.TWIN_SHARE, date, 1);
        assertNotNull(room, "Is expecting a room, as no rooms has been booked before");

        //1. hotel booking tests goes here
        long confNo = hotel.book(room, guest, date, 1, 2, card);
        IOUtils.outputln(guest.getName() + " has booked a room date: " + date + " \nconfirmation Number: " + confNo);

        Booking booking = hotel.findBookingByConfirmationNumber(confNo);

        assertNotNull(booking, "Booking exists for this room");

        //2. checkin tests follows here
        hotel.checkin(confNo);
        IOUtils.outputln(guest.getName() + " has checked in");

        initializeArrivalDate();  //put arrivak date

        Guest guest2 = new Guest("Lina", "Sydney", 2);

        CreditCard card2 = new CreditCard(CreditCardType.VISA, 3, 3);

        Room room2 = hotel.findAvailableRoom(RoomType.TWIN_SHARE, date, 3);

        assertNull(room2, "The room supposed to be null, when the same arrival date and room type is selected");
    }

    public void addHotelRoom(Hotel hotel) {
        hotel.addRoom(RoomType.SINGLE, 101);
        hotel.addRoom(RoomType.DOUBLE, 201);
        hotel.addRoom(RoomType.TWIN_SHARE, 301);

    }

    public static Hotel populateHotel() {
        Hotel hotel = new Hotel();

        return hotel;
    }

}
