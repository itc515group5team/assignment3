
import hotel.credit.CreditCard;
import hotel.entities.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.awt.print.Book;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.jupiter.api.Assertions.*;

public class HotelTest {
    @InjectMocks
    private Hotel hotel = new Hotel();
    @Mock
    Booking booking;
    int roomId = 1;

    RoomType roomType = RoomType.SINGLE;
    Room room = new Room(roomId, roomType);
    @Mock
    Guest guest;

    @Mock
    CreditCard creditCard;

    @Mock
    ServiceCharge serviceCharge;

    SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY");
    Date arrivalDate;

    {
        try {
            arrivalDate = format.parse("30-11-2018");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBook() {
        long confirmationNumber = hotel.book(room, guest, arrivalDate, booking.getStayLength(),booking.getNumberOfOccupants() , creditCard);
        /*The confirmation number has this format: ddMMYYYYrrr, where ddMMYYYY is the date of the booking and rrr is the room Id.*/

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(arrivalDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String numberString = String.format("%d%d%d%d", day, month, year, room.getId());

        assertEquals(Long.parseLong(numberString), confirmationNumber);

        //1. A booking should exist for the room (this method should call room.book())
        //Booking book = room.book(guest, arrivalDate, booking.getStayLength(), booking.getNumberOfOccupants(), creditCard);
        Booking bookingExist = hotel.findBookingByConfirmationNumber(confirmationNumber);

        assertNotNull(bookingExist, "Booking exists for this room");
        //assertEquals(booking, book);

        //2. The room should not be available for the specified arrivalDate and staylength
        assertNull(hotel.findAvailableRoom(roomType, arrivalDate, booking.getStayLength()),"Room is not available");

        //3. The booking should be returned from findBookingByConfirmationNumber()
        Booking bookingByConfirmationNumber = hotel.findBookingByConfirmationNumber(confirmationNumber);
        assertNotNull(bookingByConfirmationNumber, "Booking is returned");

    }

    @Test
    public void testCheckin() {
        long confirmationNumber = hotel.book(room, guest, arrivalDate, booking.getStayLength(),booking.getNumberOfOccupants() , creditCard);

        hotel.checkin(confirmationNumber);

        Booking activeBooking = hotel.findActiveBookingByRoomId(roomId);

        //1. The Booking referenced by confirmationNumber should be returned by getActiveBookingByRoomId()
        Booking bookingByConfirmationNumber = hotel.findBookingByConfirmationNumber(confirmationNumber);

        assertEquals(bookingByConfirmationNumber, activeBooking);

        //2. The Booking referenced by confirmationNumber should have a state of CHECKED_IN
        Boolean checkedIn = bookingByConfirmationNumber.isCheckedIn();

        assertEquals(true, checkedIn);
    }

    @Test
    public void testCheckOut() {
        long confirmationNumber = hotel.book(room, guest, arrivalDate, booking.getStayLength(),booking.getNumberOfOccupants() , creditCard);

        Booking bookingByConfirmationNumber = hotel.findBookingByConfirmationNumber(confirmationNumber);

        hotel.checkin(confirmationNumber);
        hotel.checkout(bookingByConfirmationNumber.getRoomId());

        Boolean checkedOut = bookingByConfirmationNumber.isCheckedOut();

        //The Booking referenced by confirmationNumber should have a state of CHECKED_OUT
        assertEquals(true, checkedOut);
    }


    @Test
    public void testAddServiceCharge() {
        long confirmationNumber = hotel.book(room, guest, arrivalDate, booking.getStayLength(),booking.getNumberOfOccupants() , creditCard);
        Booking bookingByConfirmationNumber = hotel.findBookingByConfirmationNumber(confirmationNumber);


        hotel.checkin(confirmationNumber);
        hotel.addServiceCharge(bookingByConfirmationNumber.getRoomId(), serviceCharge.getType(), serviceCharge.getCost());

        //	A ServiceCharge should have been added to the active booking.
        Booking activeBooking = hotel.findActiveBookingByRoomId(bookingByConfirmationNumber.getRoomId());

        List<ServiceCharge> serviceChargeList = activeBooking.getCharges();
        assertNotNull(serviceChargeList);

    }

}