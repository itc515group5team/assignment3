import hotel.credit.CreditCard;
import hotel.entities.Booking;
import hotel.entities.Room;
import hotel.entities.Guest;
import hotel.entities.RoomType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class RoomTest {
    @InjectMocks
    Room room = new Room(101, RoomType.SINGLE);

    @Mock
    Guest guest;
    @Mock
    CreditCard creditCard;

    SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY");
    Date arrivalDate;

    {
        try {
            arrivalDate = format.parse("01-01-0001");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void book() {
        Booking booking = room.book(guest, arrivalDate, 1, 1, creditCard);
        assertNotNull(booking);
        //returns a new Booking
        assertFalse(room.isAvailable(arrivalDate, 1));
        //isAvailable() should return false for any arrivalDates and stayLengths that clash with the new booking
        assertTrue(booking.isPending());
        //the booking state should be PENDING
    }

    @Test
    void checkin() {
        Booking booking = room.book(guest, arrivalDate, 1, 1, creditCard);
        assertTrue(room.isReady(), "the rooms state is not READY");
        //throws a RuntimeException if the rooms state is not READY
        room.checkin();
        //after checkin:
        assertTrue(room.isOccupied());
        //The rooms state should be OCCUPIED
        assertTrue(booking.isCheckedIn(), "the booking state should be CHECKED_IN");
        //the booking state should be CHECKED_IN
    }

    @Test
    void checkout() {
        Booking booking = room.book(guest, arrivalDate, 1, 1, creditCard);
        booking.checkIn();
        //before making a checkout, a checkin should be done
        assertTrue(room.isOccupied(), "the rooms state is not OCCUPIED");
        //throws a RuntimeException if the rooms state is not OCCUPIED
        room.checkout(booking);
        //after calling checkout method:
        assertTrue(room.isReady());
        //The rooms state should be READY
        assertTrue(booking.isCheckedOut());
        //the booking state should be CHECKED_OUT
    }
}