import hotel.entities.Hotel;
import hotel.entities.ServiceType;
import hotel.service.RecordServiceCTL;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.*;

class RecordServiceCTLTest {
    @Mock
    Hotel hotel = new Hotel();
    @InjectMocks
    RecordServiceCTL recordServiceCTL = new RecordServiceCTL(hotel);

    @Spy
    double cost;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void serviceDetailsEntered() {
        Throwable exception = assertThrows(RuntimeException.class,
                () -> {recordServiceCTL.serviceDetailsEntered(ServiceType.ROOM_SERVICE, cost);} );

        assertEquals("State is not service", exception.getMessage());
    }
}