import hotel.checkin.CheckinCTL;
import hotel.entities.Hotel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class CheckinCTLTest {
    boolean confirmed = true;

    @Mock
    Hotel hotel;

    @InjectMocks
    CheckinCTL checkinCTL = new CheckinCTL(hotel);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void checkInConfirmed() {
        Throwable exception = assertThrows(RuntimeException.class,
                () -> {checkinCTL.checkInConfirmed(confirmed);} );

        assertEquals("State is not Confirming", exception.getMessage());
    }
}