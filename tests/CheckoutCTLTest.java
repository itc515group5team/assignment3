import hotel.checkout.CheckoutCTL;
import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Hotel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class CheckoutCTLTest {

    @InjectMocks
    CheckoutCTL checkoutCTL = new CheckoutCTL(new Hotel());

    @Mock
    CreditCardType creditCardType;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void creditDetailsEnteredIntegerMax() {
        int cardNumber = Integer.MAX_VALUE;
        int ccv = 001;
        Throwable exception = assertThrows(RuntimeException.class,
                () -> {checkoutCTL.chargesAccepted(true);} );

        assertEquals("CheckoutCTL: roomIdEntered : bad state : null", exception.getMessage());
        // Overcoming bad state and catching exception

        Throwable exception2 = assertThrows(RuntimeException.class,
                () -> {checkoutCTL.creditDetailsEntered(creditCardType, cardNumber, ccv);} );

        assertEquals("The state is not Credit", exception2.getMessage());
        // Overcoming bad state and catching exception

        CreditCard creditCard = new CreditCard(creditCardType, cardNumber, ccv);
        assertNotNull(creditCard);
        assertFalse(new CreditAuthorizer().authorize(creditCard, 10), "Credit Card was not approved");
        //Credit card supposed to be not approved, because of the wrong authorization method
    }
    @Test
    void creditDetailsEnteredIntegerLessThanSix() {
        int cardNumber = 5;
        int ccv = 001;
        Throwable exception = assertThrows(RuntimeException.class,
                () -> {checkoutCTL.chargesAccepted(true);} );

        assertEquals("CheckoutCTL: roomIdEntered : bad state : null", exception.getMessage());
        // Overcoming bad state and catching exception

        Throwable exception2 = assertThrows(RuntimeException.class,
                () -> {checkoutCTL.creditDetailsEntered(creditCardType, cardNumber, ccv);} );

        assertEquals("The state is not Credit", exception2.getMessage());
        // Overcoming bad state and catching exception

        CreditCard creditCard = new CreditCard(creditCardType, cardNumber, ccv);
        assertNotNull(creditCard);
        assertTrue(new CreditAuthorizer().authorize(creditCard, 10), "Credit Card was not approved");
        //Credit card not approved
    }
}